"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.action = action;
var common = exports.common = {
  initial: {
    isLoaded: false,
    isLoading: false,
    data: [],
    error: null,
    loadMoreError: null
  },
  fetch: {
    error: null,
    loadMoreError: null,
    isLoading: false
  },
  request: {
    error: null,
    loadMoreError: null,
    isLoading: true
  },
  success: {
    isLoaded: true,
    isLoading: false,
    error: null
  },
  successWithPagination: function successWithPagination(response) {
    var currentData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var isRestart = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    return {
      isLoaded: true,
      isLoading: false,
      error: null,
      current_page: response.current_page,
      from: response.from,
      last_page: response.last_page,
      next_page_url: response.next_page_url,
      per_page: response.per_page,
      prev_page_url: response.prev_page_url,
      next_page: !currentData.next_page || response.next_page > currentData.next_page || isRestart ? response.next_page : currentData.next_page,
      prev_page: !currentData.prev_page || response.prev_page < currentData.prev_page || isRestart ? response.prev_page : currentData.prev_page,
      to: response.to,
      total: response.total
    };
  },
  failure: {
    isLoading: false,
    isLoaded: false,
    loadMoreError: null
  }
};

// Actions
function action(type) {
  var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  return _extends({ type: type }, payload);
}

var actionGroupFetchItems = exports.actionGroupFetchItems = function actionGroupFetchItems(_request, _success, _failure) {
  return {
    request: function request(filters) {
      return action(_request + "_REQUEST", { filters: filters });
    },
    success: function success(response) {
      return action(_success + "_SUCCESS", { response: response });
    },
    failure: function failure(error) {
      return action(_failure + "_FAILURE", { error: error });
    }
  };
};

var actionGroupFetchItem = exports.actionGroupFetchItem = function actionGroupFetchItem(_request2, _success2, _failure2) {
  return {
    request: function request(id, data) {
      return action(_request2 + "_REQUEST", { id: id, data: data });
    },
    success: function success(response) {
      return action(_success2 + "_SUCCESS", { response: response });
    },
    failure: function failure(error) {
      return action(_failure2 + "_FAILURE", { error: error });
    }
  };
};

var actionGroupCreateItem = exports.actionGroupCreateItem = function actionGroupCreateItem(_request3, _success3, _failure3) {
  return {
    request: function request(data, notification) {
      return action(_request3 + "_REQUEST", { data: data, notification: notification });
    },
    success: function success(response, initialData) {
      return action(_success3 + "_SUCCESS", { response: response, initialData: initialData });
    },
    failure: function failure(error) {
      return action(_failure3 + "_FAILURE", { error: error });
    }
  };
};

var actionGroupUpdateItem = exports.actionGroupUpdateItem = function actionGroupUpdateItem(_request4, _success4, _failure4) {
  return {
    request: function request(id, data, notification) {
      return action(_request4 + "_REQUEST", { id: id, data: data, notification: notification });
    },
    success: function success(response, initialData) {
      return action(_success4 + "_SUCCESS", { response: response, initialData: initialData });
    },
    failure: function failure(error) {
      return action(_failure4 + "_FAILURE", { error: error });
    }
  };
};

var actionGroupUpdateStatusItem = exports.actionGroupUpdateStatusItem = function actionGroupUpdateStatusItem(_request5, _success5, _failure5) {
  return {
    request: function request(data) {
      return action(_request5 + "_REQUEST", { data: data });
    },
    success: function success(response, initialData) {
      return action(_success5 + "_SUCCESS", { response: response, initialData: initialData });
    },
    failure: function failure(error) {
      return action(_failure5 + "_FAILURE", { error: error });
    }
  };
};

var actionGroupDeleteItem = exports.actionGroupDeleteItem = function actionGroupDeleteItem(_request6, _success6, _failure6) {
  return {
    request: function request(id, data, notification) {
      return action(_request6 + "_REQUEST", { id: id, data: data, notification: notification });
    },
    success: function success(response, initialData) {
      return action(_success6 + "_SUCCESS", { response: response, initialData: initialData });
    },
    failure: function failure(error) {
      return action(_failure6 + "_FAILURE", { error: error });
    }
  };
};