'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSearch = exports.getFilterFromProps = exports.getFiltersFromProps = exports.modalSelector = exports.modalPropSelector = exports.locationContainsQuery = exports.removeModalPropsToLocationObject = exports.addModalPropsToLocationObject = exports.removeParameterFromLocation = exports.constructLocationObject = exports.toggleItemInString = exports.parseQueryString = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _lodash = require('lodash');

var _index = require('./index');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var parseQueryString = exports.parseQueryString = function parseQueryString(qstr) {
  var query = {};
  if (!qstr || !qstr[0]) {
    return {};
  }
  var a = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');
  for (var i = 0; i < a.length; i++) {
    var b = a[i].split('=');
    query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
  }
  return query;
};

// Helper: Adds or remove part of a string.
var toggleItemInString = exports.toggleItemInString = function toggleItemInString(str, query) {
  if (!str) {
    return query;
  }

  var strArray = str.split(',');

  // If query is not yet in the string, add it
  if (!strArray.includes(String(query))) {
    return strArray.concat(query).join(',');
  }

  // Else, filter it out
  return strArray.filter(function (item) {
    return item !== String(query);
  }).join(',');
};

var convertSearchToObject = function convertSearchToObject(location) {
  if (location.search) {
    return parseQueryString(location.search);
  }
};

// Helper: Construct a new location object we can link to from the filter items.
var constructLocationObject = exports.constructLocationObject = function constructLocationObject(id, type, location) {
  var restart = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  return _extends({}, location, {
    search: '?' + (0, _index.serialize)(_extends({}, convertSearchToObject(location), _defineProperty({}, type, restart || !convertSearchToObject(location) ? id : toggleItemInString(convertSearchToObject(location)[type], id) || '')))
  });
};

// Helper: Remove a search parameter form the location.
var removeParameterFromLocation = exports.removeParameterFromLocation = function removeParameterFromLocation(type, location) {
  var searchObject = convertSearchToObject(location);

  var newSearch = (0, _index.serialize)(_extends({}, (0, _lodash.pickBy)(searchObject, function (value, key) {
    return key !== type;
  })));

  if (newSearch) {
    newSearch = '?' + newSearch;
  }

  return _extends({}, location, {
    search: newSearch
  });
};

// Helper: Construct a new location object we can link to from the filter items.
var addModalPropsToLocationObject = exports.addModalPropsToLocationObject = function addModalPropsToLocationObject(modalName, modalProps, location) {
  var locationWithModal = constructLocationObject(modalName, 'modal', location, true);

  return constructLocationObject(JSON.stringify(modalProps), 'modalProps', locationWithModal, true);
};

var removeModalPropsToLocationObject = exports.removeModalPropsToLocationObject = function removeModalPropsToLocationObject(modalName, location) {
  var newSearch = (0, _index.serialize)(_extends({}, (0, _lodash.pickBy)(location.query, function (value, key) {
    return !(0, _lodash.startsWith)(key, 'modal') && !(0, _lodash.startsWith)(key, 'arguments');
  })));
  if (newSearch) {
    newSearch = '?' + newSearch;
  }

  return _extends({}, location, {
    search: newSearch
  });
};

// Helper: Location contains query.
var locationContainsQuery = exports.locationContainsQuery = function locationContainsQuery(id, type, location) {
  var query = parseQueryString(location.search);

  if (!query || !query[type]) {
    return false;
  }

  // Split string into array and try to find key.
  return (0, _lodash.findIndex)(query[type].split(','), function (item) {
    return item === id;
  }) >= 0;
};

// Selectors
var modalPropSelector = exports.modalPropSelector = function modalPropSelector(props, item) {
  return props && props.history && props.history.location && props.history.location.search && parseQueryString(props.history.location.search).modalProps && JSON.parse(parseQueryString(props.history.location.search).modalProps) && JSON.parse(parseQueryString(props.history.location.search).modalProps)[item];
};

var modalSelector = exports.modalSelector = function modalSelector(props) {
  return props && props.history && props.history.location && props.history.location.search && parseQueryString(props.history.location.search) && parseQueryString(props.history.location.search).modal;
};

var getFiltersFromProps = exports.getFiltersFromProps = function getFiltersFromProps(props) {
  return props.history && props.history.location && props.history.location.search && (0, _lodash.pickBy)(parseQueryString(props.history.location.search), function (value, key) {
    return !(0, _lodash.startsWith)(key, 'modal') && !(0, _lodash.startsWith)(key, 'arguments');
  });
};

var getFilterFromProps = exports.getFilterFromProps = function getFilterFromProps(props, id) {
  return getFiltersFromProps(props) && getFiltersFromProps(props)[id];
};

var getSearch = exports.getSearch = function getSearch(props) {
  return props.history && props.history.location && props.history.location.search && parseQueryString(props.history.location.search) && parseQueryString(props.history.location.search).search;
};

exports.default = constructLocationObject;