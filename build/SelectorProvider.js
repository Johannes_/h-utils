'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash');

var _index = require('./index');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Gotta love ES6.
 *
 * As symbols are unique, if we attempt to pass in a new symbol with
 * the same string we will get an error as symbols are unique.
 * This enables proper encapsulation with syntax
 * that is less verbose and cleaner.
 *
 * @type {Symbol}
 * @private
 */
// eslint-disable-next-line
var _getProperty = Symbol('_getProperty');

/**
 * _module shouldn't be settable outside this class.
 *
 * @type {Symbol}
 * @private
 */
// eslint-disable-next-line
var _module = Symbol('module');

/**
 * @class SelectorProvider
 * @classdesc Provides selectors for a Redux module.
 *
 * @example
 * import SelectorProvider from 'utils/SelectorProvider';
 * const moduleName = 'state.module';
 * export const selectors = new SelectorProvider(moduleName);
 *
 * Access a selector in component: module.selectors.getItems(state);
 *
 * @readonly
 * @version 1.0.0
 */

var SelectorProvider = function () {
  /**
   * @constructor
   * @param {string} module - The module name.
   */
  function SelectorProvider(module) {
    _classCallCheck(this, SelectorProvider);

    this[_module] = module;
    (0, _index.autobind)(this, /^[a-zA-Z]/);
  }

  /**
   * Returns the bound module.
   * Currently only used for test purposes.
   *
   * @getter
   * @returns {string}
   */


  _createClass(SelectorProvider, [{
    key: _getProperty,


    /**
     * Getter method. Accessing deeply nested
     * properties is supported.
     *
     * @access private
     * @param {object} state - Redux state
     * @param {null|string} key - Eg: data.prop.deeperProp
     * @return {object|array|undefined} - Fetched property
     */
    value: function value(state, key) {
      var module = (0, _lodash.get)(state, this[_module]);
      return key ? (0, _lodash.get)(module, key) : module;
    }

    /**
     * Returns module state.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {object|array}
     */

  }, {
    key: 'getItems',
    value: function getItems(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'data';

      return this[_getProperty](state, key);
    }

    /**
     * Finds an item by id. Also supports
     * fetching a property within
     * an item object.
     *
     * @param {object} state
     * @param {string|number} id
     * @param {null|string} key
     * @returns {object|undefined}
     */

  }, {
    key: 'getItem',
    value: function getItem(state, id) {
      var key = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      var items = this.getItems(state);
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var item = _step.value;

          if (String(item.id) === String(id)) {
            return key ? item[key] : item;
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return undefined;
    }

    /**
     * Returns the meta object.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getMeta',
    value: function getMeta(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'meta';

      return this[_getProperty](state, key);
    }

    /**
     * Returns the filters.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getFilters',
    value: function getFilters(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'filters';

      return this[_getProperty](state, key);
    }

    /**
     * Returns the previous page.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getPreviousPage',
    value: function getPreviousPage(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'prev_page';

      return this[_getProperty](state, key);
    }

    /**
     * Returns the current page.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getCurrentPage',
    value: function getCurrentPage(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'current_page';

      return this[_getProperty](state, key);
    }

    /**
     * Returns the next page.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getNextPage',
    value: function getNextPage(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'next_page';

      return this[_getProperty](state, key);
    }

    /**
     * Returns the last page.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'getLastPage',
    value: function getLastPage(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'last_page';

      return this[_getProperty](state, key);
    }

    /**
     * Checks whether the last page has been reached.
     *
     * @param {object} state
     * @returns {boolean}
     */

  }, {
    key: 'isLastPage',
    value: function isLastPage(state) {
      return !this.getCurrentPage(state) || !this.getLastPage(state) || this.getCurrentPage(state) === this.getLastPage(state);
    }

    /**
     * Checks if state is currently loading.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'isLoading',
    value: function isLoading(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'isLoading';

      return this[_getProperty](state, key);
    }

    /**
     * Checks if state was loaded.
     *
     * @param {object} state
     * @param {null|string} key
     * @returns {*}
     */

  }, {
    key: 'isLoaded',
    value: function isLoaded(state) {
      var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'isLoaded';

      return this[_getProperty](state, key);
    }

    /**
     * Checks if an item is fetched.
     *
     * @param {object} state
     * @param {string|number} id
     * @returns {boolean}
     */

  }, {
    key: 'isFetched',
    value: function isFetched(state, id) {
      var item = this.getItem(state, id);
      return (0, _lodash.size)(item) !== 0;
    }

    /**
     * Finds an entity by id. Also supports
     * fetching a property within
     * an item object.
     *
     * @param {object} state
     * @param {string|number} id
     * @param {null|string} key
     * @returns {object|undefined}
     */

  }, {
    key: 'getEntity',
    value: function getEntity(state, id) {
      var key = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      var items = this.getItems(state);
      var itemCount = Object.keys(items).length;

      for (var i = 0; i < itemCount; i++) {
        var itemKey = Object.keys(items)[i];

        if (itemKey === 'entity-' + id) {
          var item = items[itemKey];

          return key ? item[key] : item;
        }
      }

      return undefined;
    }

    /**
     * Checks if an item is fetched.
     *
     * @param {object} state
     * @param {string|number} id
     * @returns {boolean}
     */

  }, {
    key: 'isEntityFetched',
    value: function isEntityFetched(state, id) {
      var item = this.getEntity(state, id);
      return (0, _lodash.size)(item) !== 0;
    }

    /**
     * Checks if an item is fetched.
     *
     * @param {object} state
     * @param {string|number} id
     * @returns {false || array}
     */

  }, {
    key: 'getEntities',
    value: function getEntities(state, id) {
      var item = this.getEntity(state, id);
      return (0, _lodash.size)(item) > 0 && item;
    }

    /**
     * Checks if item with id exists in array and returns the value.
     *
     * @param {object} state
     * @param {string|number} id
     * @returns {object|undefined}
     */

  }, {
    key: 'getItemById',
    value: function getItemById(state, id) {
      find(this.getItems(state), { id: Number(id) });
    }

    /**
     * Checks if an item with id is fetched.
     *
     * @param {object} state
     * @param {string|number} id
     * @returns {boolean}
     */

  }, {
    key: 'isItemFetched',
    value: function isItemFetched(state, id) {
      Boolean(this.getItemById(state, id));
    }
  }, {
    key: 'getModule',
    get: function get() {
      return this[_module];
    }
  }]);

  return SelectorProvider;
}();

exports.default = SelectorProvider;