'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouter = require('react-router');

var _index = require('./index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routeModal = function routeModal(modalName, PassedComponent) {
  var modalHoc = function modalHoc(props) {
    var activeModal = props.activeModal;

    // eslint-disable-next-line

    var handleForceClose = function handleForceClose(e) {
      if (e && typeof e.preventDefault === 'function') {
        e.preventDefault();
      }

      var activeModal = props.activeModal,
          _props$history = props.history,
          push = _props$history.push,
          location = _props$history.location;

      var updatedLocation = location;

      // Remove the modal prop from the url
      if (activeModal) {
        updatedLocation = (0, _index.removeModalPropsToLocationObject)(activeModal, updatedLocation);
      }

      if (location !== updatedLocation) {
        push(updatedLocation);
      }
    };

    return _react2.default.createElement(PassedComponent, _extends({}, props, {
      handleForceClose: handleForceClose,
      isOpen: activeModal === modalName
    }));
  };

  return (0, _reactRouter.withRouter)(
  // eslint-disable-next-line
  (0, _reactRedux.connect)(function (state, props) {
    return {
      activeModal: (0, _index.modalSelector)(props, 'modal')
    };
  })(modalHoc));
};

exports.default = routeModal;