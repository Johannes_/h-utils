'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTranslation = exports.tryToTranslate = exports.filterVariablesByFields = exports.getDateOfToday = exports.dateOfToday = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _lodash = require('lodash');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dateOfToday = exports.dateOfToday = (0, _moment2.default)().format('DD-MM-YYYY');

var getDateOfToday = exports.getDateOfToday = function getDateOfToday() {
  var today = new Date();

  var dd = today.getDate();
  var mm = today.getDate();
  var yyyy = today.getFullYear();

  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;

  return dd + '-' + mm + '-' + yyyy;
};

var filterVariablesByFields = exports.filterVariablesByFields = function filterVariablesByFields(data, registeredFields) {
  if ((typeof registeredFields === 'undefined' ? 'undefined' : _typeof(registeredFields)) === 'object') {
    return (0, _lodash.pick)(data, (0, _lodash.map)(registeredFields, function (field) {
      return field.name;
    }));
  }
  return (0, _lodash.pick)(data, registeredFields.map(function (field) {
    return field.name;
  }));
};

// Only usable in componentWillUpdate.
var formNeedsToReinitialize = function formNeedsToReinitialize(data, nextData, registeredFields) {
  // No fields means that the componentWillMount should initialize.
  if (!registeredFields) {
    return false;
  }

  // See if the values need to be reinitialized according to the incoming data.
  // This is only when the data object is switched, so we check the id.
  return data.id !== nextData.id;
};

var tryToTranslate = exports.tryToTranslate = function tryToTranslate(intl, key, messages, fallback) {
  if (messages[key]) {
    return intl.formatMessage(messages[key]);
  }
  return fallback || key;
};

var getTranslation = exports.getTranslation = function getTranslation(content, intl) {
  // Set the fallback first.
  var translation = content;

  // Front-end translations.
  if (content && content.id) {
    translation = intl.formatMessage(content);
  }

  // Back-end translations.
  if (content && content.code) {
    translation = intl.formatMessage({
      id: content.code,
      defaultMessage: content.message
    });
  }

  return translation;
};

exports.default = formNeedsToReinitialize;