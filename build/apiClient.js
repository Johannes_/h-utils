'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var API_ROOT = process.env.API_PATH || '/dashboard/api';

var apiClient = exports.apiClient = function apiClient(_ref) {
  var url = _ref.url,
      _ref$data = _ref.data,
      data = _ref$data === undefined ? {} : _ref$data,
      _ref$contentType = _ref.contentType,
      contentType = _ref$contentType === undefined ? 'application/json' : _ref$contentType,
      _ref$apiRoot = _ref.apiRoot,
      apiRoot = _ref$apiRoot === undefined ? API_ROOT : _ref$apiRoot;

  data.headers = {
    // eslint-disable-line
    Accept: 'application/json'
  };

  if (contentType) {
    data.headers['Content-Type'] = contentType; // eslint-disable-line
  }

  data.credentials = 'same-origin'; // eslint-disable-line

  return fetch(apiRoot + url, data).then(function (response) {
    return response.json().then(function (json) {
      return { json: json, response: response };
    });
  }).catch(function (e) {
    return { json: e, response: {} };
  }).then(function (_ref2) {
    var json = _ref2.json,
        response = _ref2.response;

    if (!response.ok && response.status === 404) {
      if (window.Raven) {
        window.Raven.captureException('API - failed to resolve', {
          extra: {
            requestUrl: apiRoot + url,
            requestData: JSON.stringify(data),
            response: JSON.stringify(json)
          }
        });
        // eslint-disable-next-line
        Raven.showReportDialog();
      }
      return { error: json };
      // return Promise.reject({ error: json, response: { content: json, response: 'test' } });
    } else if (!response.ok) {
      if (window.Raven && response.status !== 422) {
        window.Raven.captureException('API - failed to resolve', {
          extra: {
            requestUrl: apiRoot + url,
            requestData: JSON.stringify(data),
            response: JSON.stringify(json)
          }
        });
        // eslint-disable-next-line
        Raven.showReportDialog();
      }
      return json;
    }
    return { content: json, response: response };
  }).then(function (response) {
    return { response: response };
  });
};

/**
 * Adds files to request payload
 * @param inputName {string} Name of key passed in payload
 * @param files {array, object}
 * @param data {object} Other data that should be added to whole payload - optional when passing only file, required when form has other fields
 * @returns {object} FormData object
 */
var addFiles = exports.addFiles = function addFiles(inputName, files) {
  var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  var formData = new FormData();
  if (files) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = files[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var file = _step.value;

        formData.append(inputName, file);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }
  if (data) {
    for (var _inputName in data) {
      formData.append(_inputName, JSON.stringify(data[_inputName]));
    }
  }
  return formData;
};

var serialize = exports.serialize = function serialize(obj) {
  var str = [];
  for (var p in obj) {
    // eslint-disable-line
    if (obj.hasOwnProperty(p) && obj[p] !== undefined) {
      // eslint-disable-line
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  }

  return str.join('&');
};

var sirMocksALot = exports.sirMocksALot = function sirMocksALot(data, meta) {
  // eslint-disable-next-line
  return new Promise(function (resolve) {
    setTimeout(function () {
      resolve({
        response: {
          response: {},
          content: {
            data: data,
            meta: meta
          }
        }
      });
    }, 1000);
  });
};

// Generic
var updateStatus = exports.updateStatus = function updateStatus(_ref3) {
  var object = _ref3.object,
      id = _ref3.id,
      status = _ref3.status;

  return apiClient({
    url: '/' + object + '/' + id + '/status/' + status,
    data: {
      method: 'POST'
    }
  });
};

// Airlines
var fetchAirlines = exports.fetchAirlines = function fetchAirlines(query) {
  return apiClient({ url: '/airlines?' + serialize(query) });
};

// Portals
var fetchPortals = exports.fetchPortals = function fetchPortals(query) {
  return apiClient({ url: '/portals?' + serialize(query) });
};

// Airports
var fetchAirports = exports.fetchAirports = function fetchAirports(query) {
  return apiClient({ url: '/airports?' + serialize(query) });
};

// Employees
var fetchEmployees = exports.fetchEmployees = function fetchEmployees(query) {
  return apiClient({ url: '/users?' + serialize(query) });
};