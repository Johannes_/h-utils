'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isAfterFieldValue = exports.isBeforeFieldValue = exports.flightCodeValidator = exports.phoneValidator = exports.emailValidator = exports.alphaNumeric = exports.numberValidator = exports.dateValidator = exports.minLength = exports.maxLength = exports.fieldValueGreaterThanArrayValue = exports.fieldValueLessThanArrayValue = exports.fieldValueLessThanCompareValue = exports.fieldValueGreaterThanCompareValue = exports.fieldRequiredArrayValue = exports.fieldRequiredValue = exports.defaultRequired = exports.required = exports.messageWithValues = exports.messageWithValue = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /* eslint react/no-multi-comp: "off" */


var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactIntl = require('react-intl');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _commonMessages = require('./assets/i18n/commonMessages');

var _commonMessages2 = _interopRequireDefault(_commonMessages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Takes a translation object (message) and an number (length)
 * Will put the value of length in the defaultMessage {length} placeholder and
 * return the translated string
 *
 *   toLong: {
 *     id: 'validation-to-long',
 *     defaultMessage: 'This field has a max length of {length} characters'
 *    },
 *
 * @param {object} message
 * @param {string|number} length
 */
var messageWithValue = exports.messageWithValue = function messageWithValue(message, length) {
  return _react2.default.createElement(_reactIntl.FormattedMessage, _extends({}, message, { values: { length: length } }));
};

/**
 * Takes a translation object (message), the fieldValue and the compareFieldValue
 * Will put the value of fieldValue and the value of compareFieldValue in the defaultMessage {fieldValue} and {compareFieldValue} placeholders and
 * return the translated string
 *
 *   fieldValueGreaterThanCompareValue: {
 *    id: 'validation-field-value-greater-than-compare-value',
 *    defaultMessage:
 *      '{fieldValue} cannot be greater than/before {compareFieldValue}'
 *   }
 *
 * @param {object} message
 * @param {string|number} fieldValue
 * @param {string|number} compareFieldValue
 */
var messageWithValues = exports.messageWithValues = function messageWithValues(message, fieldValue, compareFieldValue) {
  return _react2.default.createElement(_reactIntl.FormattedMessage, _extends({}, message, {
    values: { fieldValue: fieldValue, compareFieldValue: compareFieldValue }
  }));
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message
 * and check if there is a value, if there is no value
 * it will return an error message (translation object)
 *
 * @param {object} message
 */
var required = exports.required = function required() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return function (fieldValue) {
    if (typeof fieldValue === 'number' && fieldValue === 0 || typeof fieldValue === 'boolean' && fieldValue === false) {
      return undefined;
    }
    return fieldValue ? undefined : message || _commonMessages2.default.required;
  };
};

/**
 * Use this const if you get an infinite loop with field level validation
 * sometimes the required instance should be created once outside of the render function
 * you cant always call a function with paramaters, also you can define a const in the file where you
 * want to perform the validations and than assign the validation function to that const and use the constant to
 * validate
 *
 * https://redux-form.com/7.1.2/examples/fieldlevelvalidation/
 */
var defaultRequired = exports.defaultRequired = required(_commonMessages2.default.required);

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (requiredValue)
 * if there is a requiredValue for example start_time and fieldValue (end_time)
 * is not set (undefined), it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} requiredValue
 */
var fieldRequiredValue = exports.fieldRequiredValue = function fieldRequiredValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var requiredValue = arguments[1];
  return function (fieldValue, allValues) {
    return allValues[requiredValue] && !fieldValue ? message ? message : _commonMessages2.default.required : undefined;
  };
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message,
 * the fieldArray name and
 * the name of another input field (requiredValue)
 * if there is a requiredValue for example start_time and fieldValue (end_time)
 * is not set (undefined), it will return an error message (translation object)
 *
 * @param {string} arrayName
 * @param {object} message
 * @param {string} requiredValue
 */
var fieldRequiredArrayValue = exports.fieldRequiredArrayValue = function fieldRequiredArrayValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var arrayName = arguments[1];
  var requiredValue = arguments[2];
  return function (fieldValue, allValues, props, fieldName) {
    var error = false;
    allValues[arrayName].forEach(function (item, key) {
      if (item && fieldName.includes('[' + key + ']') && item[requiredValue] && !fieldValue) {
        error = message ? message : _commonMessages2.default.required;
      }
    });
    return error;
  };
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is greater than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} compareValue
 */
var fieldValueGreaterThanCompareValue = exports.fieldValueGreaterThanCompareValue = function fieldValueGreaterThanCompareValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var compareValue = arguments[1];
  return function (fieldValue, allValues) {
    return allValues[compareValue] < fieldValue ? message ? message : messageWithValues(_commonMessages2.default.fieldValueGreaterThanCompareValue, fieldValue, allValues[compareValue]) : undefined;
  };
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is less than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} compareValue
 */
var fieldValueLessThanCompareValue = exports.fieldValueLessThanCompareValue = function fieldValueLessThanCompareValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var compareValue = arguments[1];
  return function (fieldValue, allValues) {
    return allValues[compareValue] > fieldValue ? message ? message : messageWithValues(_commonMessages2.default.fieldValueLessThanCompareValue, fieldValue, allValues[compareValue]) : undefined;
  };
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message, the fieldArray name and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is less than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} arrayName
 * @param {string} compareValue
 */
var fieldValueLessThanArrayValue = exports.fieldValueLessThanArrayValue = function fieldValueLessThanArrayValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var arrayName = arguments[1];
  var compareValue = arguments[2];
  return function (fieldValue, allValues, props, fieldName) {
    var error = false;
    allValues[arrayName].forEach(function (item, key) {
      if (item && fieldName.includes('[' + key + ']') && item[compareValue] > fieldValue) {
        error = message ? message : messageWithValues(_commonMessages2.default.fieldValueLessThanCompareValue, fieldValue, item[compareValue]);
      }
    });
    return error;
  };
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message, the fieldArray name and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is greater than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} arrayName
 * @param {string} compareValue
 */
var fieldValueGreaterThanArrayValue = exports.fieldValueGreaterThanArrayValue = function fieldValueGreaterThanArrayValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var arrayName = arguments[1];
  var compareValue = arguments[2];
  return function (fieldValue, allValues, props, fieldName) {
    var error = false;
    allValues[arrayName].forEach(function (item, key) {
      if (item && fieldName.includes('[' + key + ']') && item[compareValue] < fieldValue) {
        error = message ? message : messageWithValues(_commonMessages2.default.fieldValueGreaterThanCompareValue, fieldValue, item[compareValue]);
      }
    });
    return error;
  };
};

/**
 * Takes an number (max)
 * it will check if there is a value and if the value is less than max length,
 * if not it will return an error message (translation object)
 *
 * @param {number} max
 */
var maxLength = exports.maxLength = function maxLength(max) {
  return function (value) {
    return value && value.length > max ? messageWithValue(_commonMessages2.default.toLong, max) : undefined;
  };
};

/**
 * Takes an number (min)
 * it will check if there is a value, and if the value is greater than min length,
 * if not it will return an error message (translation object)
 *
 * @param {number} min
 */
var minLength = exports.minLength = function minLength(min) {
  return function (value) {
    return value && value.length < min ? messageWithValue(_commonMessages2.default.toShort, min) : undefined;
  };
};

/**
 *  Needs showTime and onlyTime (by default false)
 *  as parameters to validate the dateFormat
 *
 * Takes a value (date)
 * check if value is a valid date
 * if not it will return an error message (translation object)
 *
 * @param {boolean} showTime
 * @param {boolean} onlyTime
 */
var dateValidator = exports.dateValidator = function dateValidator(_ref) {
  var _ref$showTime = _ref.showTime,
      showTime = _ref$showTime === undefined ? false : _ref$showTime,
      _ref$onlyTime = _ref.onlyTime,
      onlyTime = _ref$onlyTime === undefined ? false : _ref$onlyTime;
  return function (value) {
    if (value) {
      if (value && onlyTime && (0, _moment2.default)(value, 'HH:mm', true).isValid()) {
        return undefined;
      } else if (value && onlyTime && (0, _moment2.default)(value, 'HH:mm:ss', true).isValid()) {
        return undefined;
      } else if (value && showTime && (0, _moment2.default)(value, 'YYYY-MM-DD HH:mm', true).isValid()) {
        return undefined;
      } else if (value && showTime && (0, _moment2.default)(value, 'YYYY-MM-DD HH:mm:ss', true).isValid()) {
        return undefined;
      } else if (value && !showTime && !onlyTime && (0, _moment2.default)(value, 'YYYY-MM-DD', true).isValid()) {
        return undefined;
      }
      return _commonMessages2.default.validationInvalidDate;
    }
  };
};

/**
 * Takes a value (number)
 * check if value is a number
 * if not it will return an error message (translation object)
 *
 * @param {number} value
 */
var numberValidator = exports.numberValidator = function numberValidator(value) {
  return value && typeof value !== 'number' ? _commonMessages2.default.mustBeNumber : undefined;
};

/**
 * Takes a value (string)
 * check if value is alphaNumeric
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
var alphaNumeric = exports.alphaNumeric = function alphaNumeric(value) {
  return value && /[^a-zA-Z0-9 ]/i.test(value) ? _commonMessages2.default.mustBeAlphanumeric : undefined;
};

/**
 * Takes a value (string)
 * check if value is a valid email
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
var emailValidator = exports.emailValidator = function emailValidator(value) {
  return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? _commonMessages2.default.validationInvalidEmail : undefined;
};

/**
 * Takes a value (string)
 * check if value is a valid email
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
var phoneValidator = exports.phoneValidator = function phoneValidator(value) {
  return value && !/^(?=(?:\D*\d){10,15}\D*$)\+?[0-9]{1,3}[\s-]?(?:\(0?[0-9]{1,5}\)|[0-9]{1,5})[-\s]?[0-9][\d\s-]{5,7}\s?(?:x[\d-]{0,4})?$/g.test(value) ? _commonMessages2.default.validationInvalidPhoneNumber : undefined;
};

var flightCodeValidator = exports.flightCodeValidator = function flightCodeValidator(value) {
  return value && !/^((?:[A-Za-z]{2,3})|(?:[A-Za-z]\\d)|(?:\\d[A-Za-z]))\\s*?(\\d{1,})([A-Za-z]?)$/.test(value) ? 'Invalid flight code' : undefined;
};

var isBeforeFieldValue = exports.isBeforeFieldValue = function isBeforeFieldValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var compareValue = arguments[1];
  return function (fieldValue, allValues) {
    var field = allValues.date + ' ' + fieldValue;
    var compare = allValues.date + ' ' + allValues[compareValue];
    var momentFieldValue = (0, _moment2.default)(field, 'YYYY-MM-DD HH:mm');
    var momentCompareValue = (0, _moment2.default)(compare, 'YYYY-MM-DD HH:mm');
    return (0, _moment2.default)(momentCompareValue).isBefore((0, _moment2.default)(momentFieldValue), 'hour') ? message ? message : messageWithValues(_commonMessages2.default.fieldValueLessThanCompareValue, fieldValue, allValues[compareValue]) : undefined;
  };
};

var isAfterFieldValue = exports.isAfterFieldValue = function isAfterFieldValue() {
  var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var compareValue = arguments[1];
  return function (fieldValue, allValues) {
    var field = allValues.date + ' ' + fieldValue;
    var compare = allValues.date + ' ' + allValues[compareValue];
    var momentFieldValue = (0, _moment2.default)(field, 'YYYY-MM-DD HH:mm');
    var momentCompareValue = (0, _moment2.default)(compare, 'YYYY-MM-DD HH:mm');
    return (0, _moment2.default)(momentCompareValue).isAfter((0, _moment2.default)(momentFieldValue), 'hour') ? message ? message : messageWithValues(_commonMessages2.default.fieldValueGreaterThanCompareValue, fieldValue, allValues[compareValue]) : undefined;
  };
};