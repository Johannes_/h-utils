'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function isRequiredUpdateObject(o) {
  return Array.isArray(o) || o && o.constructor === Object.prototype.constructor;
}

function deepDiff(o1, o2, p) {
  var notify = function notify(status) {
    console.warn('Update %s', status);
    console.log('%cbefore', 'font-weight: bold', o1);
    console.log('%cafter ', 'font-weight: bold', o2);
  };
  if (!(0, _lodash.isEqual)(o1, o2)) {
    console.group(p);
    if ([o1, o2].every(_lodash.isFunction)) {
      notify('avoidable?');
    } else if (![o1, o2].every(isRequiredUpdateObject)) {
      notify('required.');
    } else {
      var oKeys = (0, _lodash.union)((0, _lodash.keys)(o1), (0, _lodash.keys)(o2));
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = oKeys[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var key = _step.value;

          deepDiff(o1[key], o2[key], key);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
    console.groupEnd();
  } else if (o1 !== o2) {
    console.group(p);
    notify('avoidable!');
    if ((0, _lodash.isObject)(o1) && (0, _lodash.isObject)(o2)) {
      var _oKeys = (0, _lodash.union)((0, _lodash.keys)(o1), (0, _lodash.keys)(o2));
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = _oKeys[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var _key = _step2.value;

          deepDiff(o1[_key], o2[_key], _key);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    }
    console.groupEnd();
  }
}

/**
 * Wrap it around the connect or withRouter.
 * It works by hooking into the componentDidUpdate lifecycle of components.
 * This is executed after a component has been re-rendered and receives the state and props it was previously rendered with.
 * By performing a deep comparison of the data, it can determine whether the component really needed to update and log the results to the console.
 *
 * http://rea.tech/reactjs-performance-debugging-aka-the-magic-of-reselect-selectors/
 *
 * @param {object} Component
 */

var isRerendering = function isRerendering(Component) {
  var isRerendering = function (_React$Component) {
    _inherits(isRerendering, _React$Component);

    function isRerendering() {
      _classCallCheck(this, isRerendering);

      return _possibleConstructorReturn(this, (isRerendering.__proto__ || Object.getPrototypeOf(isRerendering)).apply(this, arguments));
    }

    _createClass(isRerendering, [{
      key: 'componentDidUpdate',
      value: function componentDidUpdate(prevProps, prevState) {
        deepDiff({ props: prevProps, state: prevState }, { props: this.props, state: this.state }, Component.displayName);
      }
    }, {
      key: 'render',
      value: function render() {
        return _react2.default.createElement(Component, this.props);
      }
    }]);

    return isRerendering;
  }(_react2.default.Component);

  return isRerendering;
};

exports.default = isRerendering;