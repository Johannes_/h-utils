'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = autobind;
/*
	Bind an object's methods to itself, if they match the regular expression.

	By default binds any method that starts with "on" or "handle" followed by a capital letter.
 */

// Keep track of method names.
// eslint-disable-next-line
var bound = new Set();
// eslint-disable-next-line
var notBound = new Set();

function autobind(object) {
  var regexp = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : /^(on|handle)[A-Z]/;

  var properties = Object.getOwnPropertyNames(object.constructor.prototype);

  properties.forEach(function (method) {
    if (typeof object[method] !== 'function') {
      return;
    }

    if (regexp.exec(method) === null) {
      notBound.add(method);

      return;
    }

    /* eslint-disable no-param-reassign */
    object[method] = object[method].bind(object);

    bound.add(method);
  });
}