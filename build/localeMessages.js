'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.translateLanguage = exports.translateCountry = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactIntl = require('react-intl');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint react/no-multi-comp: "off" */
var messages = (0, _reactIntl.defineMessages)({
  'locale-language-fr': {
    id: 'locale-language-fr',
    defaultMessage: 'French'
  },
  'locale-language-en': {
    id: 'locale-language-en',
    defaultMessage: 'English'
  },
  'locale-language-nl': {
    id: 'locale-language-nl',
    defaultMessage: 'Dutch'
  },
  'locale-language-de': {
    id: 'locale-language-de',
    defaultMessage: 'German'
  },
  'locale-language-be': {
    id: 'locale-language-be',
    defaultMessage: 'Belgian'
  },
  'locale-language-es': {
    id: 'locale-language-es',
    defaultMessage: 'Spanish'
  },
  'locale-language-it': {
    id: 'locale-language-it',
    defaultMessage: 'Italian'
  },
  'locale-language-dk': {
    id: 'locale-language-dk',
    defaultMessage: 'Danish'
  },
  'locale-language-pl': {
    id: 'locale-language-pl',
    defaultMessage: 'Polish'
  },

  'locale-country-FR': {
    id: 'locale-country-FR',
    defaultMessage: 'France'
  },
  'locale-country-GB': {
    id: 'locale-country-GB',
    defaultMessage: 'United Kingdom'
  },
  'locale-country-NL': {
    id: 'locale-country-NL',
    defaultMessage: 'Netherlands'
  },
  'locale-country-DE': {
    id: 'locale-country-DE',
    defaultMessage: 'Germany'
  },
  'locale-country-BE': {
    id: 'locale-country-BE',
    defaultMessage: 'Belgium'
  },
  'locale-country-ES': {
    id: 'locale-country-ES',
    defaultMessage: 'Spain'
  },
  'locale-country-IT': {
    id: 'locale-country-IT',
    defaultMessage: 'Italy'
  },
  'locale-country-DK': {
    id: 'locale-country-DK',
    defaultMessage: 'Denmark'
  },
  'locale-country-PL': {
    id: 'locale-country-PL',
    defaultMessage: 'Poland'
  }
});

var translateCountry = exports.translateCountry = function translateCountry(code, name) {
  var intl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  if (messages['locale-country-' + code]) {
    return intl ? intl.formatMessage(messages['locale-country-' + code]) : _react2.default.createElement(_reactIntl.FormattedMessage, messages['locale-country-' + code]);
  }

  return name || code;
};

var translateLanguage = exports.translateLanguage = function translateLanguage(code, name) {
  var intl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  if (messages['locale-language-' + code]) {
    return intl ? intl.formatMessage(messages['locale-language-' + code]) : _react2.default.createElement(_reactIntl.FormattedMessage, messages['locale-language-' + code]);
  }

  return name || code;
};

exports.default = messages;