'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = passPropsToChildren;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Clones children of a component
 * and passes props to it.
 *
 * @export
 * @param {React.Children} children
 * @param {object} props
 * @returns React.Children
 */
function passPropsToChildren(children, props) {
  if ((typeof props === 'undefined' ? 'undefined' : _typeof(props)) !== 'object') {
    throw new TypeError('Invalid props parameter. Props must be an object');
  }

  return _react2.default.Children.map(children, function (child) {
    return _react2.default.cloneElement(child, props);
  });
}