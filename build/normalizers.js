'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Helper. Normalizes float/integer values.
 * Accepts comma's and points.
 *
 * @param value
 * @returns {string}
 */
var normalizeFloat = exports.normalizeFloat = function normalizeFloat(value) {
  if (!value || typeof value !== 'string') return value;

  var onlyNums = value.replace(/[^\d,.]/g, '');
  return onlyNums;
};

var normalizeDate = exports.normalizeDate = function normalizeDate(value, previousValue) {
  if (!value || typeof value !== 'string') {
    return value;
  }
  var onlyNums = value.replace(/[^\d]/g, '');

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return onlyNums + '-';
    }
    if (onlyNums.length === 4) {
      return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2) + '-';
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2);
  }
  return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8);
};

var normalizeDateTime = exports.normalizeDateTime = function normalizeDateTime(value, previousValue) {
  if (!value || typeof value !== 'string') {
    return value;
  }
  var onlyNums = value.replace(/[^\d]/g, '');

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return onlyNums + '-';
    }
    if (onlyNums.length === 4) {
      return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2) + '-';
    }
    if (onlyNums.length === 6) {
      return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8);
    }
    if (onlyNums.length === 8) {
      return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8) + ' ' + onlyNums.slice(8, 10);
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2);
  }
  if (onlyNums.length <= 8) {
    return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8);
  }
  if (onlyNums.length <= 10) {
    return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8) + ' ' + onlyNums.slice(8, 10);
  }
  return onlyNums.slice(0, 2) + '-' + onlyNums.slice(2, 4) + '-' + onlyNums.slice(4, 8) + ' ' + onlyNums.slice(8, 10) + ':' + onlyNums.slice(10, 12);
};

var normalizePostalCode = exports.normalizePostalCode = function normalizePostalCode(value) {
  return value && value.replace(/[^a-zA-Z0-9-\s]/g, '');
};

var normalizeTime = exports.normalizeTime = function normalizeTime(value, previousValue) {
  if (!value || typeof value !== 'string') {
    return value;
  }
  var onlyNums = value.replace(/[^\d]/g, '');

  var onlyHours = onlyNums.slice(0, 2);
  var onlyMinutes = onlyNums.slice(2, 4);

  if (onlyHours >= 24) {
    onlyHours = 23;
  }
  if (onlyMinutes >= 60) {
    onlyMinutes = 59;
  }

  onlyNums = '' + onlyHours + onlyMinutes;

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return onlyNums + ':';
    }
    if (onlyNums.length === 4) {
      return onlyNums.slice(0, 2) + ':' + onlyNums.slice(2);
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return onlyNums.slice(0, 2) + ':' + onlyNums.slice(2);
  }

  return onlyNums.slice(0, 2) + ':' + onlyNums.slice(2, 4);
};

var normalizeFlightCode = exports.normalizeFlightCode = function normalizeFlightCode(value) {
  return value.replace(/[\s\W]/g, '');
};

var truncateString = exports.truncateString = function truncateString(value, previousValue, maxLength) {
  if (value.length > maxLength) {
    return value.substr(0, maxLength);
  } else {
    return value;
  }
};

var normalizeDateString = exports.normalizeDateString = function normalizeDateString(dateString) {
  // If it's not at least 6 characters long (8/8/88), give up.
  if (dateString.length && dateString.length < 6) {
    return '';
  }

  var date = new Date(dateString),
      month = void 0,
      day = void 0;

  // If input format was in UTC time, adjust it to local.
  if (date.getHours() || date.getMinutes()) {
    date.setMinutes(date.getTimezoneOffset());
  }

  month = date.getMonth() + 1;
  day = date.getDate();

  // Return empty string for invalid dates
  if (!day) {
    return '';
  }

  // Return the normalized string.
  return date.getFullYear() + '-' + (month > 9 ? '' : '0') + month + '-' + (day > 9 ? '' : '0') + day;
};

var normalizeAmount = exports.normalizeAmount = function normalizeAmount(amount) {
  return Number(amount).toFixed(2);
};

/*
 * Phone normalizer
 * Allows only digits and lonely + - ( )
 */
var normalizePhone = exports.normalizePhone = function normalizePhone(value) {
  return value.replace(/[^-+()0-9]/g, '').replace(/(\D)(\D)/g, '$1');
};