import { defineMessages } from 'react-intl';

/* Common messages used throughout the application */
export default defineMessages({
  required: {
    id: 'validation-required',
    defaultMessage: 'This field is required'
  },
  toShort: {
    id: 'validation-to-short',
    defaultMessage: 'This field must have more than {length} characters'
  },
  toLong: {
    id: 'validation-to-long',
    defaultMessage: 'This field has a max length of {length} characters'
  },
  mustBeNumber: {
    id: 'validation-value-must-be-number',
    defaultMessage: 'The value must be a number'
  },
  mustBeAlphanumeric: {
    id: 'validation-value-must-be-alphanumeric',
    defaultMessage: 'Only alphanumeric characters allowed'
  },
  validationInvalidEmail: {
    id: 'validation-value-invalid-email',
    defaultMessage: 'Invalid email address'
  },
  validationInvalidPhoneNumber: {
    id: 'validation-value-invalid-phone-number',
    defaultMessage: 'Invalid phone number'
  },
  validationInvalidDate: {
    id: 'validation-value-invalid-date',
    defaultMessage: 'Invalid date/time format'
  },
  fieldValueGreaterThanCompareValue: {
    id: 'validation-field-value-greater-than-compare-value',
    defaultMessage:
      '{fieldValue} cannot be greater than/before {compareFieldValue}'
  },
  fieldValueLessThanCompareValue: {
    id: 'validation-field-value-less-than-compare-value',
    defaultMessage: '{fieldValue} cannot be less than/after {compareFieldValue}'
  },
  submitButtonText: {
    id: 'submit-button-text',
    defaultMessage: 'Submit'
  },
  cancelButtonText: {
    id: 'cancel-button-text',
    defaultMessage: 'Cancel'
  },
  closeButtonText: {
    id: 'close-button-text',
    defaultMessage: 'Close'
  },
  clearButtonText: {
    id: 'clear-button-text',
    defaultMessage: 'Clear'
  },
  uploadButtonText: {
    id: 'upload-button-text',
    defaultMessage: 'Upload'
  },
  warningOnCancel: {
    id: 'phased-form-discard-warning',
    defaultMessage: 'Are you sure you want to cancel? All data will be lost.'
  },
  continue: {
    id: 'phased-form-continue-confirm-button',
    defaultMessage: 'Continue'
  },
  cancel: {
    id: 'phased-form-cancel-confirm-button',
    defaultMessage: 'Discard'
  },
  continueButton: {
    id: 'continue-work-button-text',
    defaultMessage: 'Continue work'
  },
  discardButton: {
    id: 'close-form-button-text',
    defaultMessage: 'Close form'
  },
  selectOneOptionPlaceholder: {
    id: 'select-one-option-placeholder',
    defaultMessage: 'Please select one option'
  },
  atLeastOneOfOptionShouldBeChosen: {
    id: 'validation-error-at-least-one-of-option-should-be-chosen',
    defaultMessage: 'At least one of option should be chosen'
  },
  errorAmountIsRequired: {
    id: 'validation-error-amount-is-required',
    defaultMessage: 'Amount is required'
  },

  // Placeholders
  amountPlaceholder: {
    id: 'amount-placeholder',
    defaultMessage: 'Amount in format: 123,45'
  }
});
