import React from 'react';
/**
 * Clones children of a component
 * and passes props to it.
 *
 * @export
 * @param {React.Children} children
 * @param {object} props
 * @returns React.Children
 */
export default function passPropsToChildren(children, props) {
  if (typeof props !== 'object') {
    throw new TypeError('Invalid props parameter. Props must be an object');
  }

  return React.Children.map(children, child => {
    return React.cloneElement(child, props);
  });
}
