import { pickBy, startsWith, findIndex } from 'lodash';

import { serialize } from './index'; 

export const parseQueryString = qstr => {
  const query = {};
  if (!qstr || !qstr[0]) {
    return {};
  }
  let a = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');
  for (let i = 0; i < a.length; i++) {
    let b = a[i].split('=');
    query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
  }
  return query;
};

// Helper: Adds or remove part of a string.
export const toggleItemInString = (str, query) => {
  if (!str) {
    return query;
  }

  const strArray = str.split(',');

  // If query is not yet in the string, add it
  if (!strArray.includes(String(query))) {
    return strArray.concat(query).join(',');
  }

  // Else, filter it out
  return strArray.filter(item => item !== String(query)).join(',');
};

const convertSearchToObject = location => {
  if (location.search) {
    return parseQueryString(location.search);
  }
};

// Helper: Construct a new location object we can link to from the filter items.
export const constructLocationObject = (
  id,
  type,
  location,
  restart = false
) => ({
  ...location,
  search:
    '?' +
    serialize({
      ...convertSearchToObject(location),
      [type]:
        restart || !convertSearchToObject(location)
          ? id
          : toggleItemInString(convertSearchToObject(location)[type], id) || ''
    })
});

// Helper: Remove a search parameter form the location.
export const removeParameterFromLocation = (type, location) => {
  const searchObject = convertSearchToObject(location);

  let newSearch = serialize({
    ...pickBy(searchObject, (value, key) => {
      return key !== type;
    })
  });

  if (newSearch) {
    newSearch = `?${newSearch}`;
  }

  return {
    ...location,
    search: newSearch
  };
};

// Helper: Construct a new location object we can link to from the filter items.
export const addModalPropsToLocationObject = (
  modalName,
  modalProps,
  location
) => {
  const locationWithModal = constructLocationObject(
    modalName,
    'modal',
    location,
    true
  );

  return constructLocationObject(
    JSON.stringify(modalProps),
    'modalProps',
    locationWithModal,
    true
  );
};

export const removeModalPropsToLocationObject = (modalName, location) => {
  let newSearch = serialize({
    ...pickBy(location.query, (value, key) => {
      return !startsWith(key, 'modal') && !startsWith(key, 'arguments');
    })
  });
  if (newSearch) {
    newSearch = `?${newSearch}`;
  }

  return {
    ...location,
    search: newSearch
  };
};

// Helper: Location contains query.
export const locationContainsQuery = (id, type, location) => {
  const query = parseQueryString(location.search);

  if (!query || !query[type]) {
    return false;
  }

  // Split string into array and try to find key.
  return findIndex(query[type].split(','), item => item === id) >= 0;
};

// Selectors
export const modalPropSelector = (props, item) =>
  props &&
  props.history &&
  props.history.location &&
  props.history.location.search &&
  parseQueryString(props.history.location.search).modalProps &&
  JSON.parse(parseQueryString(props.history.location.search).modalProps) &&
  JSON.parse(parseQueryString(props.history.location.search).modalProps)[item];

export const modalSelector = props =>
  props &&
  props.history &&
  props.history.location &&
  props.history.location.search &&
  parseQueryString(props.history.location.search) &&
  parseQueryString(props.history.location.search).modal;

export const getFiltersFromProps = props =>
  props.history &&
  props.history.location &&
  props.history.location.search &&
  pickBy(parseQueryString(props.history.location.search), (value, key) => {
    return !startsWith(key, 'modal') && !startsWith(key, 'arguments');
  });

export const getFilterFromProps = (props, id) =>
  getFiltersFromProps(props) && getFiltersFromProps(props)[id];

export const getSearch = props =>
  props.history &&
  props.history.location &&
  props.history.location.search &&
  parseQueryString(props.history.location.search) &&
  parseQueryString(props.history.location.search).search;

export default constructLocationObject;
