/**
 * Helper. Normalizes float/integer values.
 * Accepts comma's and points.
 *
 * @param value
 * @returns {string}
 */
export const normalizeFloat = value => {
  if (!value || typeof value !== 'string') return value;

  let onlyNums = value.replace(/[^\d,.]/g, '');
  return onlyNums;
};

export const normalizeDate = (value, previousValue) => {
  if (!value || typeof value !== 'string') {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return `${onlyNums}-`;
    }
    if (onlyNums.length === 4) {
      return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2)}-`;
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2)}`;
  }
  return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
    4,
    8
  )}`;
};

export const normalizeDateTime = (value, previousValue) => {
  if (!value || typeof value !== 'string') {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return `${onlyNums}-`;
    }
    if (onlyNums.length === 4) {
      return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2)}-`;
    }
    if (onlyNums.length === 6) {
      return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
        4,
        8
      )}`;
    }
    if (onlyNums.length === 8) {
      return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
        4,
        8
      )} ${onlyNums.slice(8, 10)}`;
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2)}`;
  }
  if (onlyNums.length <= 8) {
    return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
      4,
      8
    )}`;
  }
  if (onlyNums.length <= 10) {
    return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
      4,
      8
    )} ${onlyNums.slice(8, 10)}`;
  }
  return `${onlyNums.slice(0, 2)}-${onlyNums.slice(2, 4)}-${onlyNums.slice(
    4,
    8
  )} ${onlyNums.slice(8, 10)}:${onlyNums.slice(10, 12)}`;
};

export const normalizePostalCode = value => {
  return value && value.replace(/[^a-zA-Z0-9-\s]/g, '');
};

export const normalizeTime = (value, previousValue) => {
  if (!value || typeof value !== 'string') {
    return value;
  }
  let onlyNums = value.replace(/[^\d]/g, '');

  let onlyHours = onlyNums.slice(0, 2);
  let onlyMinutes = onlyNums.slice(2, 4);

  if (onlyHours >= 24) {
    onlyHours = 23;
  }
  if (onlyMinutes >= 60) {
    onlyMinutes = 59;
  }

  onlyNums = `${onlyHours}${onlyMinutes}`;

  if (!previousValue || value.length > previousValue.length) {
    // typing forward
    if (onlyNums.length === 2) {
      return `${onlyNums}:`;
    }
    if (onlyNums.length === 4) {
      return `${onlyNums.slice(0, 2)}:${onlyNums.slice(2)}`;
    }
  }
  if (onlyNums.length <= 2) {
    return onlyNums;
  }
  if (onlyNums.length <= 4) {
    return `${onlyNums.slice(0, 2)}:${onlyNums.slice(2)}`;
  }

  return `${onlyNums.slice(0, 2)}:${onlyNums.slice(2, 4)}`;
};

export const normalizeFlightCode = value => value.replace(/[\s\W]/g, '');

export const truncateString = (value, previousValue, maxLength) => {
  if (value.length > maxLength) {
    return value.substr(0, maxLength);
  } else {
    return value;
  }
};

export const normalizeDateString = dateString => {
  // If it's not at least 6 characters long (8/8/88), give up.
  if (dateString.length && dateString.length < 6) {
    return '';
  }

  let date = new Date(dateString),
    month,
    day;

  // If input format was in UTC time, adjust it to local.
  if (date.getHours() || date.getMinutes()) {
    date.setMinutes(date.getTimezoneOffset());
  }

  month = date.getMonth() + 1;
  day = date.getDate();

  // Return empty string for invalid dates
  if (!day) {
    return '';
  }

  // Return the normalized string.
  return (
    date.getFullYear() +
    '-' +
    (month > 9 ? '' : '0') +
    month +
    '-' +
    (day > 9 ? '' : '0') +
    day
  );
};

export const normalizeAmount = amount => Number(amount).toFixed(2);

/*
 * Phone normalizer
 * Allows only digits and lonely + - ( )
 */
export const normalizePhone = value =>
  value.replace(/[^-+()0-9]/g, '').replace(/(\D)(\D)/g, '$1');
