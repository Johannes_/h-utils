export const common = {
  initial: {
    isLoaded: false,
    isLoading: false,
    data: [],
    error: null,
    loadMoreError: null
  },
  fetch: {
    error: null,
    loadMoreError: null,
    isLoading: false
  },
  request: {
    error: null,
    loadMoreError: null,
    isLoading: true
  },
  success: {
    isLoaded: true,
    isLoading: false,
    error: null
  },
  successWithPagination: (response, currentData = {}, isRestart = false) => ({
    isLoaded: true,
    isLoading: false,
    error: null,
    current_page: response.current_page,
    from: response.from,
    last_page: response.last_page,
    next_page_url: response.next_page_url,
    per_page: response.per_page,
    prev_page_url: response.prev_page_url,
    next_page:
      !currentData.next_page ||
      response.next_page > currentData.next_page ||
      isRestart
        ? response.next_page
        : currentData.next_page,
    prev_page:
      !currentData.prev_page ||
      response.prev_page < currentData.prev_page ||
      isRestart
        ? response.prev_page
        : currentData.prev_page,
    to: response.to,
    total: response.total
  }),
  failure: {
    isLoading: false,
    isLoaded: false,
    loadMoreError: null
  }
};

// Actions
export function action(type, payload = {}) {
  return { type, ...payload };
}

export const actionGroupFetchItems = (request, success, failure) => ({
  request: filters => action(`${request}_REQUEST`, { filters }),
  success: response => action(`${success}_SUCCESS`, { response }),
  failure: error => action(`${failure}_FAILURE`, { error })
});

export const actionGroupFetchItem = (request, success, failure) => ({
  request: (id, data) => action(`${request}_REQUEST`, { id, data }),
  success: response => action(`${success}_SUCCESS`, { response }),
  failure: error => action(`${failure}_FAILURE`, { error })
});

export const actionGroupCreateItem = (request, success, failure) => ({
  request: (data, notification) =>
    action(`${request}_REQUEST`, { data, notification }),
  success: (response, initialData) =>
    action(`${success}_SUCCESS`, { response, initialData }),
  failure: error => action(`${failure}_FAILURE`, { error })
});

export const actionGroupUpdateItem = (request, success, failure) => ({
  request: (id, data, notification) =>
    action(`${request}_REQUEST`, { id, data, notification }),
  success: (response, initialData) =>
    action(`${success}_SUCCESS`, { response, initialData }),
  failure: error => action(`${failure}_FAILURE`, { error })
});

export const actionGroupUpdateStatusItem = (request, success, failure) => ({
  request: data => action(`${request}_REQUEST`, { data }),
  success: (response, initialData) =>
    action(`${success}_SUCCESS`, { response, initialData }),
  failure: error => action(`${failure}_FAILURE`, { error })
});

export const actionGroupDeleteItem = (request, success, failure) => ({
  request: (id, data, notification) =>
    action(`${request}_REQUEST`, { id, data, notification }),
  success: (response, initialData) =>
    action(`${success}_SUCCESS`, { response, initialData }),
  failure: error => action(`${failure}_FAILURE`, { error })
});
