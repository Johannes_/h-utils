/*
	Bind an object's methods to itself, if they match the regular expression.

	By default binds any method that starts with "on" or "handle" followed by a capital letter.
 */

// Keep track of method names.
// eslint-disable-next-line
const bound = new Set();
// eslint-disable-next-line
const notBound = new Set();

export default function autobind(object, regexp = /^(on|handle)[A-Z]/) {
  const properties = Object.getOwnPropertyNames(object.constructor.prototype);

  properties.forEach(method => {
    if (typeof object[method] !== 'function') {
      return;
    }

    if (regexp.exec(method) === null) {
      notBound.add(method);

      return;
    }

    /* eslint-disable no-param-reassign */
    object[method] = object[method].bind(object);

    bound.add(method);
  });
}
