const API_ROOT = process.env.API_PATH || '/dashboard/api';

export const apiClient = ({
  url,
  data = {},
  contentType = 'application/json',
  apiRoot = API_ROOT
}) => {
  data.headers = {
    // eslint-disable-line
    Accept: 'application/json'
  };

  if (contentType) {
    data.headers['Content-Type'] = contentType; // eslint-disable-line
  }

  data.credentials = 'same-origin'; // eslint-disable-line

  return fetch(apiRoot + url, data)
    .then(response => response.json().then(json => ({ json, response })))
    .catch(e => {
      return { json: e, response: {} };
    })
    .then(({ json, response }) => {
      if (!response.ok && response.status === 404) {
        if (window.Raven) {
          window.Raven.captureException('API - failed to resolve', {
            extra: {
              requestUrl: apiRoot + url,
              requestData: JSON.stringify(data),
              response: JSON.stringify(json)
            }
          });
          // eslint-disable-next-line
          Raven.showReportDialog();
        }
        return { error: json };
        // return Promise.reject({ error: json, response: { content: json, response: 'test' } });
      } else if (!response.ok) {
        if (window.Raven && response.status !== 422) {
          window.Raven.captureException('API - failed to resolve', {
            extra: {
              requestUrl: apiRoot + url,
              requestData: JSON.stringify(data),
              response: JSON.stringify(json)
            }
          });
          // eslint-disable-next-line
          Raven.showReportDialog();
        }
        return json;
      }
      return { content: json, response };
    })
    .then(response => ({ response }));
};

/**
 * Adds files to request payload
 * @param inputName {string} Name of key passed in payload
 * @param files {array, object}
 * @param data {object} Other data that should be added to whole payload - optional when passing only file, required when form has other fields
 * @returns {object} FormData object
 */
export const addFiles = (inputName, files, data = null) => {
  const formData = new FormData();
  if (files) {
    for (let file of files) {
      formData.append(inputName, file);
    }
  }
  if (data) {
    for (let inputName in data) {
      formData.append(inputName, JSON.stringify(data[inputName]));
    }
  }
  return formData;
};

export const serialize = obj => {
  const str = [];
  for (var p in obj) {
    // eslint-disable-line
    if (obj.hasOwnProperty(p) && obj[p] !== undefined) {
      // eslint-disable-line
      str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
    }
  }

  return str.join('&');
};

export const sirMocksALot = (data, meta) => {
  // eslint-disable-next-line
  return new Promise(resolve => {
    setTimeout(() => {
      resolve({
        response: {
          response: {},
          content: {
            data: data,
            meta: meta
          }
        }
      });
    }, 1000);
  });
};

// Generic
export const updateStatus = ({ object, id, status }) => {
  return apiClient({
    url: `/${object}/${id}/status/${status}`,
    data: {
      method: 'POST'
    }
  });
};

// Airlines
export const fetchAirlines = query => {
  return apiClient({ url: `/airlines?${serialize(query)}` });
};

// Portals
export const fetchPortals = query => {
  return apiClient({ url: `/portals?${serialize(query)}` });
};

// Airports
export const fetchAirports = query => {
  return apiClient({ url: `/airports?${serialize(query)}` });
};

// Employees
export const fetchEmployees = query => {
  return apiClient({ url: `/users?${serialize(query)}` });
};
