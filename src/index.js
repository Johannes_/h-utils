import { default as autobindCollection } from './autobind';
import { default as formHelpersCollection,dateOfToday as dateOfTodayFunction, getDateOfToday as getDateOfTodayFunction, filterVariablesByFields as filterVariablesByFieldsFunction, tryToTranslate as tryToTranslateFunction, getTranslation as getTranslationFunction } from './formHelpers';
import { default as isRerenderingCollection } from './isRerendering';
import { default as localeMessagesCollection, translateCountry as translateCountryFunction, translateLanguage as translateLanguageFunction } from './localeMessages';
import { default as normalizersCollection, normalizeFloat as normalizeFloatFunction, normalizeDate as normalizeDateFunction, normalizeDateTime as normalizeDateTimeFunction, normalizePostalCode as normalizePostalCodeFunction, normalizeTime as normalizeTimeFunction, normalizeFlightCode as normalizeFlightCodeFunction, truncateString as truncateStringFunction, normalizeDateString as normalizeDateStringFunction, normalizeAmount as normalizeAmountFunction, normalizePhone as normalizePhoneFunction } from './normalizers';
import { default as passPropsToChildrenCollection } from './passPropsToChildren';
import { default as reduxHelpersCollection, actionGroupFetchItems as actionGroupFetchItemsFunction, actionGroupFetchItem as actionGroupFetchItemFunction, actionGroupCreateItem as actionGroupCreateItemFunction, actionGroupUpdateItem as actionGroupUpdateItemFunction, actionGroupUpdateStatusItem as actionGroupUpdateStatusItemFunction, actionGroupDeleteItem as actionGroupDeleteItemFunction } from './reduxHelpers';
import { default as RouteModalCollection } from './RouteModal';
import { default as routerHelperCollection,  parseQueryString as parseQueryStringFunction, toggleItemInString as toggleItemInStringFunction, constructLocationObject as constructLocationObjectFunction, removeParameterFromLocation as removeParameterFromLocationFunction, addModalPropsToLocationObject as addModalPropsToLocationObjectFunction, removeModalPropsToLocationObject as removeModalPropsToLocationObjectFunction, locationContainsQuery as locationContainsQueryFunction, modalPropSelector as modalPropSelectorFunction, modalSelector as modalSelectorFunction, getFiltersFromProps as getFiltersFromPropsFunction, getFilterFromProps as getFilterFromPropsFunction, getSearch as getSearchFunction } from './routerHelpers';
import { default as SelectorProviderCollection,watchIndexAction, watchShowAction, watchLoadMoreAction, watchLoadHistoryAction, watchUpdateAction, watchRestartAction, watchCreateAction, watchDeleteAction } from './SelectorProvider';
import { default as validatorsCollection, messageWithValueValidator, messageWithValuesValidator, required as requiredValidator, defaultRequired as defaultRequiredValidator, fieldRequiredValue as fieldRequiredValueValidator, fieldRequiredArrayValueValidator, fieldValueGreaterThanCompareValueValidator, fieldValueLessThanCompareValueValidator, fieldValueLessThanArrayValueValidator, fieldValueGreaterThanArrayValueValidator, maxLengthValidator, minLengthValidator, dateValidatorValidator, numberValidatorValidator, alphaNumericValidator, emailValidatorValidator, phoneValidatorValidator, flightCodeValidatorValidator, isBeforeFieldValueValidator, isAfterFieldValueValidator } from './validators';
import { apiClient as apiClientFunction, addFiles as addFilesFunction, serialize as serializeFunction, sirMocksALot as sirMocksALotFunction, updateStatus as updateStatusFunction, fetchAirlines as fetchAirlinesFunction, fetchPortals as fetchPortalsFunction, fetchAirports as fetchAirportsFunction, fetchEmployees as fetchEmployeesFunction } from './apiClient';
import fileMockFunction from './fileMock';
import fileTransformerFunction from './fileTransformer';
import getMobileOperatingSystemFunction from './getMobileOperatingSystem';

export const autobind = autobindCollection;
export const getMobileOperatingSystem = getMobileOperatingSystemFunction;
export const formHelpers = formHelpersCollection;

export const dateOfToday = dateOfTodayFunction; 
export const getDateOfToday = getDateOfTodayFunction; 
export const filterVariablesByFields = filterVariablesByFieldsFunction; 
export const tryToTranslate = tryToTranslateFunction; 
export const getTranslation = getTranslationFunction;

export const isRerendering = isRerenderingCollection;
export const localeMessages = localeMessagesCollection;
export const translateCountry = translateCountryFunction;
export const translateLanguage = translateLanguageFunction;

export const normalizers = normalizersCollection;

export const normalizeFloat = normalizeFloatFunction;
export const normalizeDate = normalizeDateFunction;
export const normalizeDateTime = normalizeDateTimeFunction;
export const normalizePostalCode = normalizePostalCodeFunction;
export const normalizeTime = normalizeTimeFunction;
export const normalizeFlightCode = normalizeFlightCodeFunction;
export const truncateString = truncateStringFunction;
export const normalizeDateString = normalizeDateStringFunction;
export const normalizeAmount = normalizeAmountFunction;
export const normalizePhone = normalizePhoneFunction;

export const passPropsToChildren = passPropsToChildrenCollection;
export const reduxHelpers = reduxHelpersCollection;

export const actionGroupFetchItems = actionGroupFetchItemsFunction; 
export const actionGroupFetchItem = actionGroupFetchItemFunction; 
export const actionGroupCreateItem = actionGroupCreateItemFunction; 
export const actionGroupUpdateItem = actionGroupUpdateItemFunction; 
export const actionGroupUpdateStatusItem = actionGroupUpdateStatusItemFunction; 
export const actionGroupDeleteItem = actionGroupDeleteItemFunction; 

export const RouteModal = RouteModalCollection;

export const routerHelper = routerHelperCollection;
export const parseQueryString = parseQueryStringFunction;
export const toggleItemInString = toggleItemInStringFunction;
export const constructLocationObject = constructLocationObjectFunction;
export const removeParameterFromLocation = removeParameterFromLocationFunction;
export const addModalPropsToLocationObject = addModalPropsToLocationObjectFunction;
export const removeModalPropsToLocationObject = removeModalPropsToLocationObjectFunction;
export const locationContainsQuery = locationContainsQueryFunction;
export const modalPropSelector = modalPropSelectorFunction;
export const modalSelector = modalSelectorFunction;
export const getFiltersFromProps = getFiltersFromPropsFunction;
export const getFilterFromProps = getFilterFromPropsFunction;
export const getSearch = getSearchFunction;

export const SelectorProvider = SelectorProviderCollection;
export const watchIndex = watchIndexAction;
export const watchShow = watchShowAction;
export const watchLoadMore = watchLoadMoreAction;
export const watchLoadHistory = watchLoadHistoryAction;
export const watchUpdate = watchUpdateAction;
export const watchRestart = watchRestartAction;
export const watchCreate = watchCreateAction;
export const watchDelete = watchDeleteAction;

export const validators = validatorsCollection;

export const messageWithValue = messageWithValueValidator;
export const messageWithValues = messageWithValuesValidator;
export const required = requiredValidator;
export const defaultRequired = defaultRequiredValidator;
export const fieldRequiredValue = fieldRequiredValueValidator;
export const fieldRequiredArrayValue = fieldRequiredArrayValueValidator;
export const fieldValueGreaterThanCompareValue = fieldValueGreaterThanCompareValueValidator;
export const fieldValueLessThanCompareValue = fieldValueLessThanCompareValueValidator;
export const fieldValueLessThanArrayValue = fieldValueLessThanArrayValueValidator;
export const fieldValueGreaterThanArrayValue = fieldValueGreaterThanArrayValueValidator;
export const maxLength = maxLengthValidator;
export const minLength = minLengthValidator;
export const dateValidator = dateValidatorValidator;
export const numberValidator = numberValidatorValidator;
export const alphaNumeric = alphaNumericValidator;
export const emailValidator = emailValidatorValidator;
export const phoneValidator = phoneValidatorValidator;
export const flightCodeValidator = flightCodeValidatorValidator;
export const isBeforeFieldValue = isBeforeFieldValueValidator;
export const isAfterFieldValue = isAfterFieldValueValidator;

export const apiClient = apiClientFunction;
export const addFiles = addFilesFunction;
export const serialize = serializeFunction;
export const sirMocksALot = sirMocksALotFunction;
export const updateStatus = updateStatusFunction;
export const fetchAirlines = fetchAirlinesFunction;
export const fetchPortals = fetchPortalsFunction;
export const fetchAirports = fetchAirportsFunction;
export const fetchEmployees = fetchEmployeesFunction;
export const fileMock = fileMockFunction;
export const fileTransformer = fileTransformerFunction;

export default autobind;
