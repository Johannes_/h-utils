import { pick, map } from 'lodash';
import moment from 'moment';

export const dateOfToday = moment().format('DD-MM-YYYY');

export const getDateOfToday = () => {
  const today = new Date();

  let dd = today.getDate();
  let mm = today.getDate();
  let yyyy = today.getFullYear();

  if (dd < 10) dd = `0${dd}`;
  if (mm < 10) mm = `0${mm}`;

  return `${dd}-${mm}-${yyyy}`;
};

export const filterVariablesByFields = (data, registeredFields) => {
  if (typeof registeredFields === 'object') {
    return pick(data, map(registeredFields, field => field.name));
  }
  return pick(data, registeredFields.map(field => field.name));
};

// Only usable in componentWillUpdate.
const formNeedsToReinitialize = (data, nextData, registeredFields) => {
  // No fields means that the componentWillMount should initialize.
  if (!registeredFields) {
    return false;
  }

  // See if the values need to be reinitialized according to the incoming data.
  // This is only when the data object is switched, so we check the id.
  return data.id !== nextData.id;
};

export const tryToTranslate = (intl, key, messages, fallback) => {
  if (messages[key]) {
    return intl.formatMessage(messages[key]);
  }
  return fallback || key;
};

export const getTranslation = (content, intl) => {
  // Set the fallback first.
  let translation = content;

  // Front-end translations.
  if (content && content.id) {
    translation = intl.formatMessage(content);
  }

  // Back-end translations.
  if (content && content.code) {
    translation = intl.formatMessage({
      id: content.code,
      defaultMessage: content.message
    });
  }

  return translation;
};

export default formNeedsToReinitialize;
