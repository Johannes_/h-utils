import React from 'react';
import { union, isEqual, isFunction, keys, isObject } from 'lodash';

function isRequiredUpdateObject(o) {
  return (
    Array.isArray(o) || (o && o.constructor === Object.prototype.constructor)
  );
}

function deepDiff(o1, o2, p) {
  const notify = status => {
    console.warn('Update %s', status);
    console.log('%cbefore', 'font-weight: bold', o1);
    console.log('%cafter ', 'font-weight: bold', o2);
  };
  if (!isEqual(o1, o2)) {
    console.group(p);
    if ([o1, o2].every(isFunction)) {
      notify('avoidable?');
    } else if (![o1, o2].every(isRequiredUpdateObject)) {
      notify('required.');
    } else {
      const oKeys = union(keys(o1), keys(o2));
      for (const key of oKeys) {
        deepDiff(o1[key], o2[key], key);
      }
    }
    console.groupEnd();
  } else if (o1 !== o2) {
    console.group(p);
    notify('avoidable!');
    if (isObject(o1) && isObject(o2)) {
      const oKeys = union(keys(o1), keys(o2));
      for (const key of oKeys) {
        deepDiff(o1[key], o2[key], key);
      }
    }
    console.groupEnd();
  }
}

/**
 * Wrap it around the connect or withRouter.
 * It works by hooking into the componentDidUpdate lifecycle of components.
 * This is executed after a component has been re-rendered and receives the state and props it was previously rendered with.
 * By performing a deep comparison of the data, it can determine whether the component really needed to update and log the results to the console.
 *
 * http://rea.tech/reactjs-performance-debugging-aka-the-magic-of-reselect-selectors/
 *
 * @param {object} Component
 */

const isRerendering = Component => {
  class isRerendering extends React.Component {
    componentDidUpdate(prevProps, prevState) {
      deepDiff(
        { props: prevProps, state: prevState },
        { props: this.props, state: this.state },
        Component.displayName
      );
    }

    render() {
      return <Component {...this.props} />;
    }
  }

  return isRerendering;
};

export default isRerendering;
