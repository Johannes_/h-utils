import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import {
  modalSelector,
  removeModalPropsToLocationObject
} from './index';

const routeModal = (modalName, PassedComponent) => {
  const modalHoc = props => {
    const { activeModal } = props;

    // eslint-disable-next-line
    const handleForceClose = e => {
      if (e && typeof e.preventDefault === 'function') {
        e.preventDefault();
      }

      const { activeModal, history: { push, location } } = props;
      let updatedLocation = location;

      // Remove the modal prop from the url
      if (activeModal) {
        updatedLocation = removeModalPropsToLocationObject(
          activeModal,
          updatedLocation
        );
      }

      if (location !== updatedLocation) {
        push(updatedLocation);
      }
    };

    return (
      <PassedComponent
        {...props}
        handleForceClose={handleForceClose}
        isOpen={activeModal === modalName}
      />
    );
  };

  return withRouter(
    // eslint-disable-next-line
    connect((state, props) => ({
      activeModal: modalSelector(props, 'modal')
    }))(modalHoc)
  );
};

export default routeModal;
