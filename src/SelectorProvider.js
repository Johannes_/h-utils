import { get, size } from 'lodash';
import { autobind } from './index';

/**
 * Gotta love ES6.
 *
 * As symbols are unique, if we attempt to pass in a new symbol with
 * the same string we will get an error as symbols are unique.
 * This enables proper encapsulation with syntax
 * that is less verbose and cleaner.
 *
 * @type {Symbol}
 * @private
 */
// eslint-disable-next-line
const _getProperty = Symbol('_getProperty');

/**
 * _module shouldn't be settable outside this class.
 *
 * @type {Symbol}
 * @private
 */
// eslint-disable-next-line
const _module = Symbol('module');

/**
 * @class SelectorProvider
 * @classdesc Provides selectors for a Redux module.
 *
 * @example
 * import SelectorProvider from 'utils/SelectorProvider';
 * const moduleName = 'state.module';
 * export const selectors = new SelectorProvider(moduleName);
 *
 * Access a selector in component: module.selectors.getItems(state);
 *
 * @readonly
 * @version 1.0.0
 */
export default class SelectorProvider {
  /**
   * @constructor
   * @param {string} module - The module name.
   */
  constructor(module) {
    this[_module] = module;
    autobind(this, /^[a-zA-Z]/);
  }

  /**
   * Returns the bound module.
   * Currently only used for test purposes.
   *
   * @getter
   * @returns {string}
   */
  get getModule() {
    return this[_module];
  }

  /**
   * Getter method. Accessing deeply nested
   * properties is supported.
   *
   * @access private
   * @param {object} state - Redux state
   * @param {null|string} key - Eg: data.prop.deeperProp
   * @return {object|array|undefined} - Fetched property
   */
  [_getProperty](state, key) {
    const module = get(state, this[_module]);
    return key ? get(module, key) : module;
  }

  /**
   * Returns module state.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {object|array}
   */
  getItems(state, key = 'data') {
    return this[_getProperty](state, key);
  }

  /**
   * Finds an item by id. Also supports
   * fetching a property within
   * an item object.
   *
   * @param {object} state
   * @param {string|number} id
   * @param {null|string} key
   * @returns {object|undefined}
   */
  getItem(state, id, key = null) {
    const items = this.getItems(state);
    for (let item of items) {
      if (String(item.id) === String(id)) {
        return key ? item[key] : item;
      }
    }

    return undefined;
  }

  /**
   * Returns the meta object.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getMeta(state, key = 'meta') {
    return this[_getProperty](state, key);
  }

  /**
   * Returns the filters.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getFilters(state, key = 'filters') {
    return this[_getProperty](state, key);
  }

  /**
   * Returns the previous page.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getPreviousPage(state, key = 'prev_page') {
    return this[_getProperty](state, key);
  }

  /**
   * Returns the current page.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getCurrentPage(state, key = 'current_page') {
    return this[_getProperty](state, key);
  }

  /**
   * Returns the next page.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getNextPage(state, key = 'next_page') {
    return this[_getProperty](state, key);
  }

  /**
   * Returns the last page.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  getLastPage(state, key = 'last_page') {
    return this[_getProperty](state, key);
  }

  /**
   * Checks whether the last page has been reached.
   *
   * @param {object} state
   * @returns {boolean}
   */
  isLastPage(state) {
    return (
      !this.getCurrentPage(state) ||
      !this.getLastPage(state) ||
      this.getCurrentPage(state) === this.getLastPage(state)
    );
  }

  /**
   * Checks if state is currently loading.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  isLoading(state, key = 'isLoading') {
    return this[_getProperty](state, key);
  }

  /**
   * Checks if state was loaded.
   *
   * @param {object} state
   * @param {null|string} key
   * @returns {*}
   */
  isLoaded(state, key = 'isLoaded') {
    return this[_getProperty](state, key);
  }

  /**
   * Checks if an item is fetched.
   *
   * @param {object} state
   * @param {string|number} id
   * @returns {boolean}
   */
  isFetched(state, id) {
    const item = this.getItem(state, id);
    return size(item) !== 0;
  }

  /**
   * Finds an entity by id. Also supports
   * fetching a property within
   * an item object.
   *
   * @param {object} state
   * @param {string|number} id
   * @param {null|string} key
   * @returns {object|undefined}
   */
  getEntity(state, id, key = null) {
    const items = this.getItems(state);
    const itemCount = Object.keys(items).length;

    for (let i = 0; i < itemCount; i++) {
      const itemKey = Object.keys(items)[i];

      if (itemKey === `entity-${id}`) {
        const item = items[itemKey];

        return key ? item[key] : item;
      }
    }

    return undefined;
  }

  /**
   * Checks if an item is fetched.
   *
   * @param {object} state
   * @param {string|number} id
   * @returns {boolean}
   */
  isEntityFetched(state, id) {
    const item = this.getEntity(state, id);
    return size(item) !== 0;
  }

  /**
   * Checks if an item is fetched.
   *
   * @param {object} state
   * @param {string|number} id
   * @returns {false || array}
   */
  getEntities(state, id) {
    const item = this.getEntity(state, id);
    return size(item) > 0 && item;
  }

  /**
   * Checks if item with id exists in array and returns the value.
   *
   * @param {object} state
   * @param {string|number} id
   * @returns {object|undefined}
   */
  getItemById(state, id) {
    find(this.getItems(state), { id: Number(id) });
  }

  /**
   * Checks if an item with id is fetched.
   *
   * @param {object} state
   * @param {string|number} id
   * @returns {boolean}
   */
  isItemFetched(state, id) {
    Boolean(this.getItemById(state, id));
  }
}
