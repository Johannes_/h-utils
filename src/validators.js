/* eslint react/no-multi-comp: "off" */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import moment from 'moment';
import messages from './assets/i18n/commonMessages';

/**
 * Takes a translation object (message) and an number (length)
 * Will put the value of length in the defaultMessage {length} placeholder and
 * return the translated string
 *
 *   toLong: {
 *     id: 'validation-to-long',
 *     defaultMessage: 'This field has a max length of {length} characters'
 *    },
 *
 * @param {object} message
 * @param {string|number} length
 */
export const messageWithValue = (message, length) => (
  <FormattedMessage {...message} values={{ length: length }} />
);

/**
 * Takes a translation object (message), the fieldValue and the compareFieldValue
 * Will put the value of fieldValue and the value of compareFieldValue in the defaultMessage {fieldValue} and {compareFieldValue} placeholders and
 * return the translated string
 *
 *   fieldValueGreaterThanCompareValue: {
 *    id: 'validation-field-value-greater-than-compare-value',
 *    defaultMessage:
 *      '{fieldValue} cannot be greater than/before {compareFieldValue}'
 *   }
 *
 * @param {object} message
 * @param {string|number} fieldValue
 * @param {string|number} compareFieldValue
 */
export const messageWithValues = (message, fieldValue, compareFieldValue) => (
  <FormattedMessage
    {...message}
    values={{ fieldValue: fieldValue, compareFieldValue: compareFieldValue }}
  />
);

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message
 * and check if there is a value, if there is no value
 * it will return an error message (translation object)
 *
 * @param {object} message
 */
export const required = (message = '') => fieldValue => {
  if (
    (typeof fieldValue === 'number' && fieldValue === 0) ||
    (typeof fieldValue === 'boolean' && fieldValue === false)
  ) {
    return undefined;
  }
  return fieldValue ? undefined : message || messages.required;
};

/**
 * Use this const if you get an infinite loop with field level validation
 * sometimes the required instance should be created once outside of the render function
 * you cant always call a function with paramaters, also you can define a const in the file where you
 * want to perform the validations and than assign the validation function to that const and use the constant to
 * validate
 *
 * https://redux-form.com/7.1.2/examples/fieldlevelvalidation/
 */
export const defaultRequired = required(messages.required);

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (requiredValue)
 * if there is a requiredValue for example start_time and fieldValue (end_time)
 * is not set (undefined), it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} requiredValue
 */
export const fieldRequiredValue = (message = '', requiredValue) => (
  fieldValue,
  allValues
) =>
  allValues[requiredValue] && !fieldValue
    ? message ? message : messages.required
    : undefined;

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message,
 * the fieldArray name and
 * the name of another input field (requiredValue)
 * if there is a requiredValue for example start_time and fieldValue (end_time)
 * is not set (undefined), it will return an error message (translation object)
 *
 * @param {string} arrayName
 * @param {object} message
 * @param {string} requiredValue
 */
export const fieldRequiredArrayValue = (
  message = '',
  arrayName,
  requiredValue
) => (fieldValue, allValues, props, fieldName) => {
  let error = false;
  allValues[arrayName].forEach((item, key) => {
    if (
      item &&
      fieldName.includes(`[${key}]`) &&
      (item[requiredValue] && !fieldValue)
    ) {
      error = message ? message : messages.required;
    }
  });
  return error;
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is greater than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} compareValue
 */
export const fieldValueGreaterThanCompareValue = (
  message = '',
  compareValue
) => (fieldValue, allValues) =>
  allValues[compareValue] < fieldValue
    ? message
      ? message
      : messageWithValues(
          messages.fieldValueGreaterThanCompareValue,
          fieldValue,
          allValues[compareValue]
        )
    : undefined;

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is less than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} compareValue
 */
export const fieldValueLessThanCompareValue = (message = '', compareValue) => (
  fieldValue,
  allValues
) =>
  allValues[compareValue] > fieldValue
    ? message
      ? message
      : messageWithValues(
          messages.fieldValueLessThanCompareValue,
          fieldValue,
          allValues[compareValue]
        )
    : undefined;

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message, the fieldArray name and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is less than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} arrayName
 * @param {string} compareValue
 */
export const fieldValueLessThanArrayValue = (
  message = '',
  arrayName,
  compareValue
) => (fieldValue, allValues, props, fieldName) => {
  let error = false;
  allValues[arrayName].forEach((item, key) => {
    if (
      item &&
      fieldName.includes(`[${key}]`) &&
      item[compareValue] > fieldValue
    ) {
      error = message
        ? message
        : messageWithValues(
            messages.fieldValueLessThanCompareValue,
            fieldValue,
            item[compareValue]
          );
    }
  });
  return error;
};

/**
 * Takes an optional translation object (message) or an empty string '' as parameter to use default message, the fieldArray name and
 * the name of another input field (compareValue)
 * if there is a compareValue for example start_time and fieldValue (end_time)
 * it will check if the fieldValue is greater than compareValue, if this is true it will return an error message (translation object)
 *
 * @param {object} message
 * @param {string} arrayName
 * @param {string} compareValue
 */
export const fieldValueGreaterThanArrayValue = (
  message = '',
  arrayName,
  compareValue
) => (fieldValue, allValues, props, fieldName) => {
  let error = false;
  allValues[arrayName].forEach((item, key) => {
    if (
      item &&
      fieldName.includes(`[${key}]`) &&
      item[compareValue] < fieldValue
    ) {
      error = message
        ? message
        : messageWithValues(
            messages.fieldValueGreaterThanCompareValue,
            fieldValue,
            item[compareValue]
          );
    }
  });
  return error;
};

/**
 * Takes an number (max)
 * it will check if there is a value and if the value is less than max length,
 * if not it will return an error message (translation object)
 *
 * @param {number} max
 */
export const maxLength = max => value =>
  value && value.length > max
    ? messageWithValue(messages.toLong, max)
    : undefined;

/**
 * Takes an number (min)
 * it will check if there is a value, and if the value is greater than min length,
 * if not it will return an error message (translation object)
 *
 * @param {number} min
 */
export const minLength = min => value =>
  value && value.length < min
    ? messageWithValue(messages.toShort, min)
    : undefined;

/**
 *  Needs showTime and onlyTime (by default false)
 *  as parameters to validate the dateFormat
 *
 * Takes a value (date)
 * check if value is a valid date
 * if not it will return an error message (translation object)
 *
 * @param {boolean} showTime
 * @param {boolean} onlyTime
 */
export const dateValidator = ({
  showTime = false,
  onlyTime = false
}) => value => {
  if (value) {
    if (value && onlyTime && moment(value, 'HH:mm', true).isValid()) {
      return undefined;
    } else if (value && onlyTime && moment(value, 'HH:mm:ss', true).isValid()) {
      return undefined;
    } else if (
      value &&
      showTime &&
      moment(value, 'YYYY-MM-DD HH:mm', true).isValid()
    ) {
      return undefined;
    } else if (
      value &&
      showTime &&
      moment(value, 'YYYY-MM-DD HH:mm:ss', true).isValid()
    ) {
      return undefined;
    } else if (
      value &&
      !showTime &&
      !onlyTime &&
      moment(value, 'YYYY-MM-DD', true).isValid()
    ) {
      return undefined;
    }
    return messages.validationInvalidDate;
  }
};

/**
 * Takes a value (number)
 * check if value is a number
 * if not it will return an error message (translation object)
 *
 * @param {number} value
 */
export const numberValidator = value =>
  value && typeof value !== 'number' ? messages.mustBeNumber : undefined;

/**
 * Takes a value (string)
 * check if value is alphaNumeric
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
export const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? messages.mustBeAlphanumeric
    : undefined;

/**
 * Takes a value (string)
 * check if value is a valid email
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
export const emailValidator = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? messages.validationInvalidEmail
    : undefined;

/**
 * Takes a value (string)
 * check if value is a valid email
 * if not it will return an error message (translation object)
 *
 * @param {string} value
 */
export const phoneValidator = value =>
  value &&
  !/^(?=(?:\D*\d){10,15}\D*$)\+?[0-9]{1,3}[\s-]?(?:\(0?[0-9]{1,5}\)|[0-9]{1,5})[-\s]?[0-9][\d\s-]{5,7}\s?(?:x[\d-]{0,4})?$/g.test(
    value
  )
    ? messages.validationInvalidPhoneNumber
    : undefined;

export const flightCodeValidator = value =>
  value &&
  !/^((?:[A-Za-z]{2,3})|(?:[A-Za-z]\\d)|(?:\\d[A-Za-z]))\\s*?(\\d{1,})([A-Za-z]?)$/.test(
    value
  )
    ? 'Invalid flight code'
    : undefined;

export const isBeforeFieldValue = (message = '', compareValue) => (
  fieldValue,
  allValues
) => {
  const field = `${allValues.date} ${fieldValue}`;
  const compare = `${allValues.date} ${allValues[compareValue]}`;
  const momentFieldValue = moment(field, 'YYYY-MM-DD HH:mm');
  const momentCompareValue = moment(compare, 'YYYY-MM-DD HH:mm');
  return moment(momentCompareValue).isBefore(moment(momentFieldValue), 'hour')
    ? message
      ? message
      : messageWithValues(
          messages.fieldValueLessThanCompareValue,
          fieldValue,
          allValues[compareValue]
        )
    : undefined;
};

export const isAfterFieldValue = (message = '', compareValue) => (
  fieldValue,
  allValues
) => {
  const field = `${allValues.date} ${fieldValue}`;
  const compare = `${allValues.date} ${allValues[compareValue]}`;
  const momentFieldValue = moment(field, 'YYYY-MM-DD HH:mm');
  const momentCompareValue = moment(compare, 'YYYY-MM-DD HH:mm');
  return moment(momentCompareValue).isAfter(moment(momentFieldValue), 'hour')
    ? message
      ? message
      : messageWithValues(
          messages.fieldValueGreaterThanCompareValue,
          fieldValue,
          allValues[compareValue]
        )
    : undefined;
};
